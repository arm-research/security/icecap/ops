let
  nixpkgs = builtins.fetchGit {
    url = "https://github.com/NixOS/nixpkgs.git";
    ref = "nixos-unstable";
    rev = "689b76bcf36055afdeb2e9852f5ecdd2bf483f87";
  };

  pkgs = import nixpkgs {};
  nixosFn = import (nixpkgs + "/nixos");

  inherit (pkgs) lib;

  icecapLocal = ../../icecap;

  icecapRemote = builtins.fetchGit {
    url = "https://gitlab.com/arm-research/security/icecap/icecap.git";
    ref = "main";
    rev = "d0c57454415d5e0aac213ec0193b00ae5da7c056";
  };

  # icecap = icecapLocal;
  icecap = icecapRemote;

  icecapModules = icecap + "/hack/ci/infrastructure-examples/config.example.nix";

  mk = { arch, cache ? false, runner ? false }: rec {
    inherit (nixos.config.system.build) toplevel;
    nixos = nixosFn {
      configuration.imports = [
        {
          nixpkgs.localSystem.system = "${arch}-linux";
        }
        icecapModules
        ./config.nix
      ] ++ lib.optionals cache [
        {
          services.nix-serve.enable = true;
          services.nix-serve.openFirewall = true;
          services.nix-serve.secretKeyFile = toString /secrets/cache-priv-key.pem;
        }
      ] ++ lib.optionals runner [
        {
          icecap.ci.gitlab-runner.enable = true;
          icecap.ci.gitlab-runner.registrationConfigFile = toString /secrets/gitlab-runner-registration-config.sh;
        }
      ];
    };
  };

in rec {
  inherit pkgs;

  arch = lib.listToAttrs (lib.forEach [ "x86_64" "aarch64" ] (arch: lib.nameValuePair arch {
    cache = mk { inherit arch; cache = true; };
    runner = mk { inherit arch; cache = true; runner = true; };
  }));
}
